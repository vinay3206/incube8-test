# InCube8-test

###Getting Started###

You Need to have node js installed to run this project.

Project is built on ReactJS and it used React-redux for state management in the app.

Tickts data is stored in local storage

Add new tickets to see the data

Features:
1. Add new ticket
2. Edit ticket
3. Move to Done, Close
4. Tickets moved to Close after 5s, once they are in Done status
5. Search tickets by there title



####Familiar with Git?#####
Checkout this repo, install dependencies, then start the process with the following:

```
	> git clone https://gitlab.com/vinay3206/incube8-test.git
	> cd incube8-test
	> yarn
	> yarn start
	> Open "localhost:3000" in your browser
```

####To Run tests#####

```
	> yarn test
```
