import {
  Container,
  Image,
  Menu,
} from 'semantic-ui-react'
import styled from 'styled-components'

const StyledImage = styled(Image)`
 margin-right: 1.5em;
`

const Navbar = () => {
  return (
    <Menu fixed='top' inverted>
      <Container>
        <Menu.Item as='a' header href="/">
          <StyledImage size='small' src='http://www.incube8.sg/wp-content/themes/incube8-theme/images/logo_small.png'/>
          Ticketify
        </Menu.Item>
        <Menu.Item as='a'>Home</Menu.Item>
      </Container>
    </Menu>
  )
}

export default Navbar