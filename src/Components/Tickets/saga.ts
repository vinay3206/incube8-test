import { put, call, takeEvery, fork, delay } from 'redux-saga/effects'
import {
  createTicketSuccess,
  createTicketFailure,
  getTicketsSuccess,
  getTicketsFailure,
  getTickets as getTicketsAction,
  createTicket, editTicket,
  editTicketSuccess,
  updateStatus,
  editTicketsFailure
} from './slice'
import { getTickets, setTickets } from '../../Utils/local-storage'
import { ICreateTicket, ITicket, IStatus, IUpdateStatus } from './types'
import { PayloadAction } from '@reduxjs/toolkit'
import { push } from 'react-router-redux'

export function* getTicketsHandler() {
  try {
    // delay of 1s to show loader on UI
    yield delay(1000)
    const res: string = yield call(getTickets)
    if (res) {
      const tickets: ITicket[] = JSON.parse(res)
      yield put(getTicketsSuccess({ tickets }))
    } else {
      yield put(getTicketsSuccess({ tickets: [] }))
    }
  } catch (e) {
    yield put(getTicketsFailure())
  }
}

export function* createTicketHanlder(action: PayloadAction<ICreateTicket>) {
  try {
    // delay of 1s to show loader on UI
    yield delay(1000)
    const res: string = yield call(getTickets)
    let tickets: ITicket[] = []
    if (res) {
      tickets = JSON.parse(res)
    }
    const newTicket = {
      ...action.payload,
      id: new Date().valueOf().toString(),
      status: IStatus.inProgress
    }
    tickets.push(newTicket)
    yield call(setTickets, JSON.stringify(tickets))
    yield put(createTicketSuccess(newTicket))
    yield put(push('/tickets'))
  } catch (e) {
    yield put(createTicketFailure())
  }
}

export function* doneStatusHandler(ticket: ITicket) {
    // move it to done after 5s
    yield delay(5000)
    const res: string = yield call(getTickets)
    if (res) {
      const tickets:ITicket[] = JSON.parse(res)
      const index = tickets.findIndex(t => t.id === ticket.id)
      if (index !== -1 && tickets[index].status === IStatus.done) {
        tickets[index].status = IStatus.close
        yield call(setTickets, JSON.stringify(tickets))
        yield put(editTicketSuccess(tickets[index]))
      }
    }
}

export function* editTicketHanlder(action: PayloadAction<ITicket>) {
  try {
    // delay of 1s to show loader on UI 
    yield delay(1000)
    const res: string = yield call(getTickets)
    if (res) {
      const tickets:ITicket[] = JSON.parse(res)
      const index = tickets.findIndex(t => t.id === action.payload.id)
      if (index !== -1) {
        tickets[index] = action.payload
      }
      yield call(setTickets, JSON.stringify(tickets))
      yield put(editTicketSuccess(action.payload))
      // if status is updated to closed then set a handler to set it as done after 5s
      if (action.payload.status === IStatus.done) {
        yield fork(doneStatusHandler, tickets[index])
      }
      yield put(push('/tickets'))
    }
  } catch (e) {
    yield put(editTicketsFailure())
  }
}

export function* satusUpdateHandler(action: PayloadAction<IUpdateStatus>) {
  try {
    const res: string = yield call(getTickets)
    if (res) {
      const tickets:ITicket[] = JSON.parse(res)
      const index = tickets.findIndex(t => t.id === action.payload.id)
      if (index !== -1) {
        tickets[index].status = action.payload.status
      }
      yield call(setTickets, JSON.stringify(tickets))
      yield put(editTicketSuccess(tickets[index]))
      // if status is updated to closed then set a handler to set it as done after 5s
      if (action.payload.status === IStatus.done) {
        yield fork(doneStatusHandler, tickets[index])
      }
    }
    
  } catch (e) {}
}

function* watchGetTickets() {
  yield takeEvery(getTicketsAction.toString(), getTicketsHandler)
}

function* watchCreateTicket() {
  yield takeEvery(createTicket.toString(), createTicketHanlder)
}

function* watchEditTicket() {
  yield takeEvery(editTicket.toString(), editTicketHanlder)
}

function* watchUpdateStatus() {
  yield takeEvery(updateStatus.toString(), satusUpdateHandler)
}

const saga = [fork(watchGetTickets), fork(watchCreateTicket), fork(watchEditTicket), fork(watchUpdateStatus)]

export default saga