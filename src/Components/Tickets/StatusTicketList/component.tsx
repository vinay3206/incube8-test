import React from 'react'
import { shallowEqual, useSelector } from 'react-redux'
import {
  Header
} from 'semantic-ui-react'
import styled from 'styled-components'
import { IStatus, ITicketState } from '../types'
import { component as Ticket }  from '../Ticket'

const StyledHeaderContent = styled(Header.Content)`
  text-transform: capitalize;
`

interface IStatusTicketListProps {
  type: IStatus
}



const StatusTicketList = ( props: IStatusTicketListProps) => {
  const tickets = useSelector(({ manageTickets }: { manageTickets: ITicketState}) => {
    return manageTickets.tickets.filter(t => t.status === props.type).map(t => t.id)
  }, shallowEqual)

  return (
    <React.Fragment>
      <Header as='h2' dividing>
        <StyledHeaderContent>{props.type}</StyledHeaderContent>
      </Header>
      {
        tickets.map(id => <Ticket id={id} key={id} />)
      }
  </React.Fragment>
)}

export default StatusTicketList