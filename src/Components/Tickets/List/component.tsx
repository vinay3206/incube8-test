import React, { useEffect } from 'react'
import { Grid, Loader } from 'semantic-ui-react'
import { component as TicketsHeader } from '../TicketListHeader'
import { component as StatusTicketList } from '../StatusTicketList'
import {  IStatus, ITicketState } from '../types'
import { shallowEqual, useDispatch, useSelector } from 'react-redux'
import { getTickets } from '../slice'



const TicketList = () => {
  const dispatch = useDispatch()
  const isFetching = useSelector(({ manageTickets }: { manageTickets: ITicketState}) => {
    return manageTickets.fetchingTickets
  }, shallowEqual)

  useEffect(() => {
    dispatch(getTickets())
  },[dispatch])

  return (
    <React.Fragment>
    <TicketsHeader />
    {
      isFetching ?
      <Loader active inline='centered' />
      :
    <Grid columns={3} divided>
      <Grid.Row>
        <Grid.Column textAlign="center">
          <StatusTicketList type={IStatus.inProgress} />
        </Grid.Column>
        <Grid.Column textAlign="center">
          <StatusTicketList type= {IStatus.done}/>
        </Grid.Column>
        <Grid.Column textAlign="center">
          <StatusTicketList type= {IStatus.close}/>
        </Grid.Column>
      </Grid.Row>
    </Grid>
  }
  </React.Fragment>
)}

export default TicketList