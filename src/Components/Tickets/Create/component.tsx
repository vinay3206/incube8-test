import React  from 'react'
import { shallowEqual, useDispatch, useSelector } from 'react-redux'
import { SubmitHandler, useForm } from 'react-hook-form'
import { RouteComponentProps } from "react-router-dom";
import { goBack } from 'react-router-redux'
import {
  Button,
  Modal,
  Icon,
  Form,
  Message,
} from 'semantic-ui-react'
import { createTicket, editTicket } from '../slice'
import { IStatus, ITicketState } from '../types'

type IFormValues = {
  title: string,
  description: string,
  status?: IStatus,
}

const CreateEditTicket = ({ match }: RouteComponentProps<{ id?: string}>) => {
  const dispatch = useDispatch()
  const isCreating = useSelector(({ manageTickets }: { manageTickets: ITicketState}) => {
    return manageTickets.isCreating
  }, shallowEqual)

  const isEditing = useSelector(({ manageTickets }: { manageTickets: ITicketState}) => {
    return manageTickets.isEditing
  }, shallowEqual)

  const ticket = useSelector(({ manageTickets }: { manageTickets: ITicketState}) => {
    return manageTickets.tickets.find( t => t.id === match.params.id)
  }, shallowEqual)

  const isEdit = !!match.params.id

  let defaultValues: IFormValues = { title: '', description: ''}

  if(ticket) {
    defaultValues = { ...ticket }
  }

  const { register, handleSubmit, formState: { errors } } = useForm<IFormValues>({ defaultValues })

  const onSubmit: SubmitHandler<IFormValues> = data => {
    if (isEdit && match.params.id && data.status) {
      dispatch(editTicket({  ...data, id: match.params.id, status: data.status,}))
    } else {
      dispatch(createTicket(data))
    }
  }

  const titleHasError = errors.title?.type === 'required'
  const descriptionHasError =  errors.description?.type === 'required'

  return (<Modal open={true}  size="tiny" onClose={() => dispatch(goBack())} closeOnDimmerClick={false}>
  <Modal.Header>
    <Icon name="ticket" /> { isEdit ? 'Edit tikeet' : 'Create new ticket' }
  </Modal.Header>
  <Modal.Content>
    <Form error={titleHasError || descriptionHasError}>
      <Message
        error
        header='Required'
        content='Please enter all required values'
      />
      <Form.Field error={titleHasError}>
        <label htmlFor="title">Title</label>
        <input {...register('title', { required: true })} />
      </Form.Field>
      <Form.Field error={descriptionHasError}>
        <label htmlFor="description">Description</label>
        <textarea  {...register('description', { required: true })} />
      </Form.Field>
      {
        isEdit ?
          <Form.Field>
        ` <label htmlFor="Status"></label>
          <select {...register('status')}>
            <option value="in-progress">In-Progress</option>
            <option value="done">Done</option>
            <option value="close">Close</option>
          </select>
          </Form.Field>
        : ''
      }
    </Form>
  </Modal.Content>
  <Modal.Actions>
    <Button
     onClick={() => dispatch(goBack())}
      default
      icon="chevron left"
      content="Go back"
      labelPosition="left"
      disabled={isCreating || isEditing}
    />
    <Button
      onClick={() => handleSubmit(onSubmit)()}
      color="orange"
      labelPosition="left"
      icon="plus"
      content="Submit"
      loading={isCreating || isEditing}
      disabled={isCreating || isEditing}
    />
  </Modal.Actions>
  </Modal>
)}

export default CreateEditTicket