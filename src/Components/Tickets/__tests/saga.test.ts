import { call, delay, fork, put } from 'redux-saga/effects'
import { push } from 'react-router-redux'
import { getTickets, setTickets } from '../../../Utils/local-storage'
import { 
   createTicketHanlder,
   doneStatusHandler, 
   editTicketHanlder, 
   getTicketsHandler, 
   satusUpdateHandler 
  } from '../saga'
import { createTicket, 
  createTicketFailure, 
  editTicket, 
  editTicketsFailure, 
  editTicketSuccess, 
  getTicketsFailure, 
  getTicketsSuccess, 
  updateStatus } from '../slice'
import { IStatus, ITicket } from '../types'

describe('getTickets handler saga testing when local storage is empty', () => {
  const generator = getTicketsHandler()
  let next = generator.next()

  it('Should call delay', () => {
    expect(next.value).toEqual(delay(1000))
  })

  it('Should call getTickets', () => {
    next = generator.next()
    expect(next.value).toEqual(call(getTickets))
  })

  it('Should call getTicketsSuccess with empty array', () => {
    next = generator.next()
    expect(next.value).toEqual(put(getTicketsSuccess({ tickets: [] })))
  })

  it('should be done now', () => {
    expect(generator.next().done).toBeTruthy()
  })
})

describe('getTickets handler saga testing when local storage is not empty', () => {
  const generator = getTicketsHandler()
  let next = generator.next()

  it('Should call delay', () => {
    expect(next.value).toEqual(delay(1000))
  })

  it('Should call getTickets', () => {
    next = generator.next()
    expect(next.value).toEqual(call(getTickets))
  })

  it('Should call getTicketsSuccess with array', () => {
    const tickets: ITicket[] = [{id: '1', status: IStatus.inProgress, title: 'test', description: 'test'}]
    const res = JSON.stringify(tickets)
    next = generator.next(res)
    expect(next.value).toEqual(put(getTicketsSuccess({ tickets })))
  })

  it('should be done now', () => {
    expect(generator.next().done).toBeTruthy()
  })
})

describe('getTickets handler saga testing when call is failed', () => {
  const generator = getTicketsHandler()
  let next = generator.next()

  it('Should call delay', () => {
    expect(next.value).toEqual(delay(1000))
  })

  it('Should call getTickets', () => {
    next = generator.next()
    expect(next.value).toEqual(call(getTickets))
  })

  it('should put getTicketsFailure', () => {
    const error = {
      message: 'failed'
    }
    next = generator.throw(error)
    expect(next.value).toEqual(put(getTicketsFailure()))
  })

  it('should be done now', () => {
    expect(generator.next().done).toBeTruthy()
  })
})

describe('createTicket handler saga testing success case', () => {
  const action = createTicket({
    title: 'test',
    description: 'test description'
  })
  const generator = createTicketHanlder(action)
  let next = generator.next()

  it('Should call delay', () => {
    expect(next.value).toEqual(delay(1000))
  })

  it('Should call getTickets', () => {
    next = generator.next()
    expect(next.value).toEqual(call(getTickets))
  })

  it('Should call setTickets with new item', () => {
    next = generator.next()
    //@ts-ignore
    expect(next.value.payload.fn).toEqual(setTickets)
  })

  it('Should put createTicketSuccess with new item', () => {
    next = generator.next()
    //@ts-ignore
    expect(next.value.payload.action.type).toEqual('manageTickets/createTicketSuccess')
  })

  it('Should push to /tickets', () => {
    next = generator.next()
    expect(next.value).toEqual(put(push('/tickets')))
  })

  it('should be done now', () => {
    expect(generator.next().done).toBeTruthy()
  })
})

describe('createTicket handler saga testing failure case', () => {
  const action = createTicket({
    title: 'test',
    description: 'test description'
  })
  const generator = createTicketHanlder(action)
  let next = generator.next()

  it('Should call delay', () => {
    expect(next.value).toEqual(delay(1000))
  })

  it('Should call getTickets', () => {
    next = generator.next()
    expect(next.value).toEqual(call(getTickets))
  })

  it('Should call setTickets with new item', () => {
    next = generator.next()
    //@ts-ignore
    expect(next.value.payload.fn).toEqual(setTickets)
  })

  it('Should put createTicketFailure', () => {
    const error = {
      message: 'failed'
    }
    next = generator.throw(error)
    expect(next.value).toEqual(put(createTicketFailure()))
  })

  it('should be done now', () => {
    expect(generator.next().done).toBeTruthy()
  })
})

describe('editTicket handler saga testing success case', () => {
  const newTicket = {
    id: '1',
    title: 'test',
    description: 'test description',
    status: IStatus.done
  }
  const action = editTicket(newTicket)
  const generator = editTicketHanlder(action)
  let next = generator.next()

  it('Should call delay', () => {
    expect(next.value).toEqual(delay(1000))
  })

  it('Should call getTickets', () => {
    next = generator.next()
    expect(next.value).toEqual(call(getTickets))
  })

  it('Should call setTickets with new item', () => {
    const storeTickets = [{ ...newTicket, status: IStatus.inProgress }]
    next = generator.next(JSON.stringify(storeTickets))
    expect(next.value).toEqual(call(setTickets, JSON.stringify([newTicket])))
  })

  it('Should put editTicktSuccess with new item', () => {
    next = generator.next()
    expect(next.value).toEqual(put(editTicketSuccess(newTicket)))
  })

  it('Should fork doneStatushandler Since payload has done status', () => {
    next = generator.next()
    expect(next.value).toEqual(fork(doneStatusHandler, newTicket))
  })

  it('Should push to /tickets', () => {
    next = generator.next()
    expect(next.value).toEqual(put(push('/tickets')))
  })

  it('should be done now', () => {
    expect(generator.next().done).toBeTruthy()
  })
})

describe('editTicket handler saga testing failure case', () => {
  const newTicket = {
    id: '1',
    title: 'test',
    description: 'test description',
    status: IStatus.done
  }
  const action = editTicket(newTicket)
  const generator = editTicketHanlder(action)
  let next = generator.next()

  it('Should call delay', () => {
    expect(next.value).toEqual(delay(1000))
  })

  it('Should call getTickets', () => {
    next = generator.next()
    expect(next.value).toEqual(call(getTickets))
  })

  it('Should put editTicketsFailure', () => {
    const error = {
      message: 'failed'
    }
    next = generator.throw(error)
    expect(next.value).toEqual(put(editTicketsFailure()))
  })

  it('should be done now', () => {
    expect(generator.next().done).toBeTruthy()
  })
})

describe('status update handler sags testing', () => {
  const payload = {
    id: '1',
    status: IStatus.done
  }
  const action = updateStatus(payload)
  const generator = satusUpdateHandler(action)
  let next = generator.next()

  it('Should call getTickets', () => {
    expect(next.value).toEqual(call(getTickets))
  })

  const storeTicket = { ...payload, status: IStatus.inProgress, title: 'test', description: 'test des' }
  const updatedTicket = { ...storeTicket, status: IStatus.done}

  it('Should call setTickets with new item', () => {
    const storeTickets = [storeTicket]
    next = generator.next(JSON.stringify(storeTickets))
    expect(next.value).toEqual(call(setTickets, JSON.stringify([updatedTicket])))
  })

  it('Should put editTicktSuccess with new item', () => {
    next = generator.next()
    expect(next.value).toEqual(put(editTicketSuccess(updatedTicket)))
  })

  it('Should fork doneStatushandler Since payload has done status', () => {
    next = generator.next()
    expect(next.value).toEqual(fork(doneStatusHandler, updatedTicket))
  })

  it('should be done now', () => {
    expect(generator.next().done).toBeTruthy()
  })
})

describe('doneStatusHandler saga testing', () => {
  const newTicket = {
    id: '1',
    title: 'test',
    description: 'test description',
    status: IStatus.done
  }
  const generator = doneStatusHandler(newTicket)
  let next = generator.next()

  it('Should call delay', () => {
    expect(next.value).toEqual(delay(5000))
  })

  it('Should call getTickets', () => {
    next = generator.next()
    expect(next.value).toEqual(call(getTickets))
  })

  const storeTicket = { ...newTicket, status: IStatus.done }
  const updatedTicket = { ...storeTicket, status: IStatus.close}

  it('Should call setTickets with new item', () => {
    const storeTickets = [storeTicket]
    next = generator.next(JSON.stringify(storeTickets))
    expect(next.value).toEqual(call(setTickets, JSON.stringify([updatedTicket])))
  })

  it('Should put editTicktSuccess with new item', () => {
    next = generator.next()
    expect(next.value).toEqual(put(editTicketSuccess(updatedTicket)))
  })

  it('should be done now', () => {
    expect(generator.next().done).toBeTruthy()
  })
})