import { createTicket, 
  createTicketFailure, 
  createTicketSuccess, 
  editTicket, 
  editTicketsFailure, 
  editTicketSuccess, 
  getTickets, 
  getTicketsFailure, 
  getTicketsSuccess, 
  searchTickets } from '../slice'
import reducer, { initialState } from '../slice'
import { IStatus, ITicket } from '../types'

describe('Get tickets  Reducer tests', () => {
  it('Should set isFetchingTickets to true', () => {
    const action = getTickets()
    expect(reducer(initialState, action)).toEqual({
      ...initialState,
      fetchingTickets: true
    })
  })

  it('Should set isFetchingTickets to false when get tickets failed', () => {
    const action = getTicketsFailure()
    expect(reducer({...initialState, fetchingTickets: true }, action)).toEqual({
      ...initialState,
      fetchingTickets: false,
      errorMessage: "Failed to fetch tickets"
    })
  })

  it('Should set isFetchingTickets to false when get tickets success', () => {
    const tickets: ITicket[] = [{id: '1', status: IStatus.inProgress, title: 'test', description: 'test'}]
    const action = getTicketsSuccess({ tickets })
    expect(reducer({...initialState, fetchingTickets: true }, action)).toEqual({
      ...initialState,
      fetchingTickets: false,
      tickets,
      backupTicketList: tickets
    })
  })
})

describe('create Ticket  Reducer tests', () => {
  it('Should set isCreating to true', () => {
    const action = createTicket({
      title: 'test',
      description: 'test description'
    })
    expect(reducer(initialState, action)).toEqual({
      ...initialState,
      isCreating: true
    })
  })

  it('Should set isCreating to false when create ticket failed', () => {
    const action = createTicketFailure()
    expect(reducer({...initialState, isCreating: true }, action)).toEqual({
      ...initialState,
      isCreating: false,
      errorMessage: "Failed to create ticket"
    })
  })

  it('Should set isCreating to false when create tickets success', () => {
    const tickets: ITicket[] = [{id: '1', status: IStatus.inProgress, title: 'test', description: 'test'}]
    const newTicket = {
      id: '2',
      status: IStatus.inProgress,
      title: 'new ticket',
      description: 'new test'
    }
    const action = createTicketSuccess(newTicket)
    const expected = [...tickets, newTicket]
    expect(reducer({ ...initialState, isCreating: true, tickets }, action)).toEqual({
      ...initialState,
      isCreating: false,
      tickets: expected,
      backupTicketList: expected
    })
  })
})

describe('edit Ticket  Reducer tests', () => {
  it('Should set isEditing to true', () => {
    const tickets: ITicket[] = [{id: '1', status: IStatus.inProgress, title: 'test', description: 'test'}]
    const action = editTicket({
      id: '1',
      title: 'test',
      description: 'test description',
      status: IStatus.close
    })
    expect(reducer({ ...initialState, isEditing: false, tickets }, action)).toEqual({
      ...initialState,
      isEditing: true,
      tickets
    })
  })

  it('Should set isEditing to false when edit ticket failed', () => {
    const action = editTicketsFailure()
    expect(reducer({...initialState, isEditing: true }, action)).toEqual({
      ...initialState,
      isEditing: false,
      errorMessage: "Failed to edit ticket"
    })
  })

  it('Should set isEditing to false when edit tickets success', () => {
    const tickets: ITicket[] = [{id: '1', status: IStatus.inProgress, title: 'test', description: 'test'}]
    const newTicket = {
      id: '1',
      status: IStatus.close,
      title: 'new ticket',
      description: 'new test'
    }
    const action = editTicketSuccess(newTicket)
    const expected = [newTicket]
    expect(reducer({ ...initialState, isEditing: true, tickets }, action)).toEqual({
      ...initialState,
      isEditing: false,
      tickets: expected,
      backupTicketList: expected
    })
  })
})

describe('Search Ticket Reducer tests', () => {
  it('Should return search results', () => {
    const tickets: ITicket[] = [
      {id: '1', status: IStatus.inProgress, title: 'test', description: 'test'}, 
      {id: '2', status: IStatus.inProgress, title: 'vinay', description: 'test'}
    ]
    const action = searchTickets({ searchTerm: 'test'})
    expect(reducer({ ...initialState, tickets, backupTicketList: tickets }, action)).toEqual({
      ...initialState,
      tickets: [tickets[0]],
      backupTicketList: tickets
    })
  })
})
