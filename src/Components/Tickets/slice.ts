import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import {
  ITicketState,
  ICreateTicket,
  IGetTicketsSuccess,
  ICreateTicketSucess,
  ITicket,
  ISearchTicket,
  IUpdateStatus,
} from './types'

export const initialState: ITicketState = {
  tickets: [],
  backupTicketList: [],
  fetchingTickets: false,
  isCreating: false,
  isEditing: false,
}

export const ticketsSlice = createSlice({
  name: 'manageTickets',
  initialState,
  reducers: {
    getTickets: (state: ITicketState) => {
      state.fetchingTickets = true
    },
    getTicketsSuccess: (state: ITicketState, action: PayloadAction<IGetTicketsSuccess>) => {
      state.fetchingTickets = false
      state.tickets = action.payload.tickets
      state.backupTicketList = action.payload.tickets
    },
    getTicketsFailure: (state:ITicketState) => {
      state.fetchingTickets = false
      state.errorMessage = "Failed to fetch tickets";  
    },
    searchTickets:(state: ITicketState, action: PayloadAction<ISearchTicket>) => {
      const { searchTerm } = action.payload
      if(searchTerm) {
        // search always runs on backup list which is not shown on UI
        state.tickets = state.backupTicketList.filter(t => t.title.includes(searchTerm))
      } else {
        // reset tickets to old list once search term is empty
        state.tickets = state.backupTicketList ? [...state.backupTicketList] : []
      }
    },
    createTicket: (state: ITicketState, action:PayloadAction<ICreateTicket>) => {
      state.isCreating = true
    },
    createTicketSuccess: (state: ITicketState, action: PayloadAction<ICreateTicketSucess>) => {
      state.tickets.push(action.payload)
      state.isCreating = false
      state.backupTicketList = state.tickets
    },
    createTicketFailure:(state: ITicketState) => {
      state.isCreating = false
      state.errorMessage = "Failed to create ticket"
    },
    editTicket: (state: ITicketState, action: PayloadAction<ITicket>) => {
      state.isEditing = true
    },
    editTicketSuccess: (state: ITicketState, action: PayloadAction<ITicket>) => {
      state.isEditing = false
      const tickets = state.tickets
      let index = tickets.findIndex(t => t.id === action.payload.id)
      if (index !== -1) {
        tickets[index] = action.payload
      }
      state.backupTicketList = state.tickets
    },
    editTicketsFailure: (state:ITicketState) => {
      state.isEditing = false
      state.errorMessage = "Failed to edit ticket";  
    },
    updateStatus: (state: ITicketState, action: PayloadAction<IUpdateStatus>) => {}
  }
})

export const {
  getTickets,
  getTicketsSuccess,
  getTicketsFailure,
  createTicket,
  createTicketSuccess,
  createTicketFailure,
  editTicket,
  editTicketSuccess,
  editTicketsFailure,
  searchTickets,
  updateStatus,
} = ticketsSlice.actions

export default ticketsSlice.reducer
