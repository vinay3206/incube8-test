import React from 'react'
import { useDispatch } from 'react-redux'
import { Link } from 'react-router-dom'
import {
  Button,
  Icon,
  Grid,
  Header,
  Input,
} from 'semantic-ui-react'
import { searchTickets } from '../slice'

const TicketsHeader = () => {
  let searchTerm = ''
  const dispatch = useDispatch()
  const onSearchTermChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    searchTerm = e.target.value
  }
  const handleKeyDown = (e: React.KeyboardEvent<HTMLInputElement>) => {
    if (e.key === 'Enter') {
      dispatch(searchTickets({ searchTerm }))
    }
  }
  return (
  <Grid stackable>
    <Grid.Column width={3}>
      <Header as="h2">
        <Icon name="options" />
        <Header.Content>
          Tickets
        <Header.Subheader>manage tickets</Header.Subheader>
      </Header.Content>
      </Header>
    </Grid.Column>
    <Grid.Column width={8}>
      <Input
        fluid
        placeholder="Search by ticket title. Press `Enter` to search."
        icon="search"
        onKeyDown={handleKeyDown}
        onChange={onSearchTermChange}
      />
    </Grid.Column>
    <Grid.Column width={5} textAlign="right">
      <Button
        as={Link}
        color="orange"
        icon="plus"
        to='/tickets/add'
        content="Add ticket"
        />
    </Grid.Column>
  </Grid>
)}

export default TicketsHeader