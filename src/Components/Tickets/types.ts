export interface ITicketState {
  tickets: ITicket[],
  backupTicketList: ITicket[],
  fetchingTickets: boolean,
  isCreating: boolean,
  isEditing: boolean,
  errorMessage?: string
}

export enum IStatus {
  inProgress = 'in-progress',
  done = 'done',
  close = 'close'
}

export interface ITicket extends ICreateTicketSucess{}

export interface IGetTicketsSuccess {
  tickets: ITicket[]
 }

export interface ICreateTicket {
 title: string,
 description: string
}

export interface ICreateTicketSucess extends  ICreateTicket{
  status: IStatus,
  id: string,
}

export interface IUpdateStatus {
  id: string,
  status: IStatus
}

export interface ISearchTicket {
  searchTerm?: string
}

export interface IMatch {
  url: string,
}