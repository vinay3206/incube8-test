import React from 'react'
import { shallowEqual, useDispatch, useSelector } from 'react-redux'
import { Link } from 'react-router-dom'
import {
  Card,
  Button,
} from 'semantic-ui-react'
import styled from 'styled-components'
import { updateStatus } from '../slice'
import { IStatus, ITicketState } from '../types'

const StyledCard = styled(Card)`
  margin-left: auto!important;
  margin-right: auto !important;
  margin-bottom: 0.5em;
`

interface ITicketProps {
  id: string,
}

const Ticket = (props: ITicketProps) => {
  const dispatch = useDispatch()
  const ticket = useSelector(({ manageTickets }: { manageTickets: ITicketState}) => {
    return manageTickets.tickets.find(t => t.id === props.id)
  }, shallowEqual)

  const onInProgress = () => dispatch(updateStatus({ id: props.id, status: IStatus.inProgress}))
  const onDone = () => dispatch(updateStatus({ id: props.id, status: IStatus.done}))
  const onClose = () => dispatch(updateStatus({ id: props.id, status: IStatus.close}))

  const color  =  ticket?.status === IStatus.done ? 'green' : (ticket?.status === IStatus.inProgress ? 'orange' : 'grey')

  if (ticket) {
    return (
      <StyledCard color={color}>
        <Card.Content>
          <Card.Header>{ticket.title}</Card.Header>
          <Card.Description>
            {ticket.description}
          </Card.Description>
        </Card.Content>
       <Card.Content extra>
        <div className='ui three buttons'>
          <Button basic color='orange' as={Link} to={`/tickets/edit/${props.id}`}>
            Edit
          </Button>
          {
            ticket.status !== IStatus.close 
            ?
              <>
                {
                  ticket.status === IStatus.inProgress
                  ?
                    <Button basic color='green' onClick={onDone}>
                      Done
                    </Button>
                  :
                    <Button basic color='red' onClick={onInProgress}>
                     Not Fix
                    </Button>
                }
                <Button basic color='black' onClick={onClose}>
                  Close
                </Button>
              </>
            : ''
          }
        </div>
      </Card.Content>
    </StyledCard>
  )} else {
    return <React.Fragment />
  }
}

export default Ticket