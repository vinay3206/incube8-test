import { Route, RouteComponentProps } from 'react-router'
import styled from 'styled-components'
import { component as TicketList } from './List'
import { component as CreateEditTicket } from './Create'

const Container = styled.div`
  padding-top: 6em;
`

 const TicketsContainer = ({ match }: RouteComponentProps<{}>) => {
  return (
    <Container>
      <Route path={`${match.url}/`} component={TicketList} />
      <Route path={`${match.url}/add`} component={CreateEditTicket} />
      <Route path={`${match.url}/edit/:id`} component={CreateEditTicket} />
    </Container>
)}

export default TicketsContainer