import Container from './Container'
import reducer from './slice'
import saga from './saga'

const reducers = {
  manageTickets: reducer
}


export { Container, reducers, saga }