import styled from 'styled-components'
import { Redirect, Route, Switch } from 'react-router'

import { Navbar } from './Components/NavBar'
import { Container as TicketPage } from './Components/Tickets';

const Container = styled.div`
  padding: 2rem 1rem 1rem 1rem;  
`

function App() {
  return (
    <div className="App">
        <Navbar />
        <Container>
          <Switch>
          <Route exact path="/">
            <Redirect to="/tickets" />
          </Route>
            <Route path="/tickets" component={TicketPage} /> 
          </Switch>
        </Container>
    </div>
  );
}

export default App;