export function setTickets(ticketsData: string) {
  localStorage.setItem('ticket_data', ticketsData)
}

export function getTickets() {
  return localStorage.getItem('ticket_data')
}