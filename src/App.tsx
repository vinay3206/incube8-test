import React from 'react';
import { ConnectedRouter } from 'connected-react-router'
import { Provider } from 'react-redux'
import history from './history'
import store from './store'
import sagaMiddleware from './sagaMiddleware'
import MainApp from './MainApp'

import sagas from './sagas'

sagaMiddleware.run(sagas)


function App() {
  return (
    <Provider store={store}>
      <ConnectedRouter history={history} >
        <MainApp />
      </ConnectedRouter> 
    </Provider>
  );
}

export default App;
