import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'
import { History } from 'history'
import { reducers as ticketsReducer } from './Components/Tickets'

const createRootReducer = (history: History) =>
  combineReducers({
    router: connectRouter(history),
    // rest of your reducers
    ...ticketsReducer,
    
  })

export default createRootReducer