import { all } from 'redux-saga/effects'
import { saga as ticketsSaga } from './Components/Tickets'

// Main Saga
export default function* rootSaga() {
  yield all([
    ...ticketsSaga
  ])
}